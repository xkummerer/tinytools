#!/usr/bin/fennel

(fn lines [s] (icollect [l _ (string.gmatch s "([^\n]+)")] l))
(fn filter [c xs] (icollect [_ x (ipairs xs)] (when (c x) x)))
(fn map [f xs] (icollect [_ x (ipairs xs)] (f x)))
(fn range [from to step] (fcollect [n from to step] n))
(fn str-quote [s] (.. "\"" s "\""))

;;; https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Text
(local scad-code "
use </home/x/font/FSEX300.ttf>
linear_extrude(6)
  text(text=%s,
       font=\"Fixedsys Excelsior 3.01\",
       size=20,
       spacing=0.7);\n")

;;; https://en.wikipedia.org/wiki/STL_(file_format)
;;; https://en.wikipedia.org/wiki/Wavefront_.obj_file
(fn txt->obj! [txt]
  (with-open [scad (io.open "/tmp/txt.scad" :w)]
    (scad:write (string.format scad-code (str-quote txt))))
  (os.execute "/usr/bin/openscad /tmp/txt.scad -o /tmp/txt.stl")
  ;; "convert" openscad stl to wavefront obj:
  (with-open [stl (io.open "/tmp/txt.stl" :r)
              obj (io.open "/tmp/txt.obj" :w)]
    (let [n (length
             (->> (lines (stl:read "*a"))
                  (filter #(string.match $ :vertex))
                  (map #(string.gsub $ "%s+vertex" "v"))
                  (map #(obj:write $ "\n"))))]
      ;; make an obj "f vn vn+1 vn+2" face statement
      ;; for every stl triangle ("facet"):
      (let [f-format "f %i %i %i\n"]
        (map
         #(obj:write (string.format f-format $ (+ $ 1) (+ $ 2)))
         (range 1 n 3))))))

(txt->obj! (or (. arg 1) "no argument"))

;;; pd-gem preview test:
;; (local s (require :socket))
;; (local tcp (s.tcp))
;; (tcp:connect "localhost" 5432)
;; (fn refresh [] (tcp:send "bang;\n"))
